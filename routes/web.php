<?php


Route::get('/', function () {
    return view('welcome');
});

Route::get('users/especial', 'UserController@especial');
Route::resource('users', 'UserController');
Route::resource('products', 'ProductController');
Route::resource('cathegories', 'CathegoryController');
Route::resource('roles', 'RoleController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/groups', 'GroupController@index');
Route::get('/groups/flush', 'GroupController@flush');
Route::get('/groups/{id}', 'GroupController@addUser');

Route::get('/basket', 'BasketController@index');
Route::get('/basket/flush', 'BasketController@flush');
Route::get('/basket/store', 'BasketController@store');
Route::get('/basket/{id}', "BasketController@addProduct");
Route::get('/basket/{id}/up', "BasketController@addProduct");
Route::get('/basket/{id}/down', "BasketController@removeProduct");

Route::get('/orders', 'OrderController@index');
Route::get('/orders/{id}', 'OrderController@show');
Route::get('/orders/{id}/update', 'OrderController@update');
Route::get("/orders/pdf/{id}","OrderController@pdf");

Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');


Route::get("/mail","MailController@index");
Route::post("/mail/send","MailController@send");
