<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cathegories') -> insert([
            "id" => 1,
            "name" => "Refrescos"
        ]);
        DB::table('cathegories') -> insert([
            "id" => 2,
            "name" => "Bocatas"
        ]);

        DB::table('products') -> insert([
            "id" => 1,
            "name" => "Jamon",
            "price" => 1.5,
            "cathegory_id" => 2
        ]);
        DB::table('products') -> insert([
            "id" => 2,
            "name" => "Queso",
            "price" => 1.5,
            "cathegory_id" => 2
        ]);
        DB::table('products') -> insert([
            "id" => 3,
            "name" => "Tomate",
            "price" => 1.5,
            "cathegory_id" => 2
        ]);
    }
}
