<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'provider', 'provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        // 1 .. n SIENDO ESTO LA PARTE 1
        return $this->belongsTo(Role::class);
        // return $this->hasOne("App\Class", foreign_Key, primary_key);

        //belongsTo 1 .. n --> Es la aprte 1
        //hasMany 1 .. n --> ES LA PARTE N

        //hasOne 1 .. 1

        //belongsToMany (en los dos) n .. n
    }
}
