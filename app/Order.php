<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\User;

class Order extends Model
{
    protected $fillable = [
        "date",'user_id'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class,'order_product')->withPivot('quantity','price');
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function total()
    {
        $total = 0;
        foreach ($this->products as $product) {
            $total += $product->pivot->quantity * $product->pivot->price;
        }
        return $total;
    }


    public function pagado()
    {
        return $this->paid == 1 ? "Pagado" : "Sin pagar";
    }
}
