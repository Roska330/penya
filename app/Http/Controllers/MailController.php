<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\SendEmail;

class MailController extends Controller
{
    public function index()
    {
        return view("mail.home");
    }

    public function send(Request $req)
    {
        $this->validate($req, [
            "email" => "required|email",
            "subject" => "required",
            "message" => "required",
        ]);



        Mail::to($req->email)->send(new SendEmail($req->subject,$req->message));

        return back()->with('success', "Gracias por su informacion");
    }
}
