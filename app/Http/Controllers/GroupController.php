<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use \Session;

class GroupController extends Controller
{
    public function index(Request $request)
    {
        //$request->session()->forget("group");
        //dd($request->session()->get("group"));
        $group = $request->session()->get("group");
        if(!$group) {
            $group = [];
        }
        return view('group.index', ['users' => $group]);
    }

    public function addUser(Request $request, $id)
    {
        // Buscar usuario
        $user = User::findOrFail($id);

        $group = $request->session()->get("group");

        if($group == null)
            $group = array();
        if(!in_array($user, $group)){
            $request->session()->push("group",$user);
        }
        return redirect("/groups");



        // Comprobar si está en el grupo de sesión

        // Si no esta, añadirlo
    }

    public function deleteUser(Request $request,$id)
    {
        $request->session()->forget("group");
    }

    public function flush(Request $request)
    {
        $request->session()->forget("group");
        return redirect("/users");
    }


}
