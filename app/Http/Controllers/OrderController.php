<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use NahidulHasan\Html2pdf\Facades\Pdf;

class OrderController extends Controller
{
    function __construct()
    {
        // Solo pueda verlo los usuarios logueados
        $this->middleware("auth");

        // Solo pueda verlo los usuarios sin loguear
        //$this->middleware("guest");
    }

    public function index()
    {
        $orders = Order::paginate(10);
        return view('order.index', ['orders' => $orders]);
    }

    public function show($id)
    {
        $order = Order::findOrFail($id);
        return view('order.show', [
            'order' => $order,
        ]);
    }


    public function update($id)
    {
        $order = Order::findOrFail($id);
        $order->paid = !$order->paid;
        $order->save();

        return redirect("/orders");
    }

    public function pdf($id)
    {
        $order = Order::findOrFail($id);
        $pdf = "<h3> Pedido: " . $order->id . "&emsp; Fecha: ". $order->date . "<br> <br> <table padding='15'> <tr><th>Producto</th> <th>Cantidad</th> <th>Precio</th></tr> <br>";

        foreach ($order->products as $product) {

            $pdf = $pdf . "<tr><td>" . $product->name . " </td><td> " . $product->pivot->quantity . " </td><td> " .
            $product->pivot->price . "&euro; </td></tr>";

        }
        $pdf = $pdf . "</table>";
        $document = Pdf::generatePdf($pdf);
        return Pdf::download();
    }

}
