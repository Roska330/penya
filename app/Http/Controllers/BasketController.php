<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;
use \Session;

class BasketController extends Controller
{
    public function index(Request $request)
    {
        //dd($request->session()->get("basket"));

        $basket = $request->session()->get("basket");
        if(!$basket)
            $basket = [];
        return view("basket.index", ["products" => $basket]);

    }

    public function addProduct(Request $request, $id)
    {

        $product = Product::findOrFail($id);

        $basket = $request->session()->get("basket");
        // dd($basket);

        if($basket == null)
            $basket = [];

        $position = -1;
        foreach($basket as $key => $item) {
            if($item->id == $id) {
                $position = $key;
                break;
            }
        }
        if($position < 0){
            $product->quantity = 1;
            $request->session()->push("basket", $product);

        } else {
            $basket[$position]->quantity++;
        }

        $basket = $request->session()->get("basket");

        if(!$basket)
            $basket = [];
        return view("basket.index", ["products" => $basket]);
    }

    public function removeProduct(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        $basket = $request->session()->get("basket");
        if($basket == null)
            $basket = [];

        $position = -1;
        foreach($basket as $key => $product) {
            if($product->id == $id) {
                $position = $key;
                break;
            }
        }

        if($position >= 0){
            if($basket[$position]->quantity > 1)
                $basket[$position]->quantity -= 1;
            else
                $request->session()->pull("basket",$product);
        }
        $basket = $request->session()->get("basket");

        if(!$basket)
            $basket = [];

        return view("basket.index", ["products" => $basket]);
    }

    public function flush(Request $request)
    {
        $request->session()->forget("basket");
        return redirect("/products");

    }

    public function store(Request $request)
    {
        $basket = $request->session()->get("basket");
        // dd($basket);

        if($basket != null) {
            $user = \Auth::user();
            $order = Order::create([
                "date"=>date("Y/m/d"),
                "paid"=>false,
                "user_id"=>$user->id]
            );
            $total = 0;
            foreach ($basket as $product) {
                $total += $product->price * $product->quantity;
            }

            foreach ($basket as $product) {
                $order->products()->attach($product->id, ['quantity' => $product->quantity, 'price' => $product->price]);
            }

        }

        $this->flush($request);

        return redirect("/products");
    }

}
