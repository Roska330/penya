@extends('layouts.app')

@section('title', 'Producto')

@section('content')
    <h1>
        Este es el detalle del producto {{ $product->id }}
    </h1>

    <ul>
        <li>Nombre: {{ $product->name }}</li>
        <li>Precio: {{ $product->price }}€</li>
        <li>Categoria: {{ $product->Cathegory()->name }}</li>
        <li>Fecha creación: {{ date("d/m/Y H:i:s", strtotime($product->created_at)) }}</li>
        <li>Fecha actualización: {{ date("d/m/Y H:i:s", strtotime($product->updated_at)) }}</li>
    </ul>

@endsection

