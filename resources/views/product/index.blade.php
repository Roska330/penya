@extends('layouts.app')

@section('content')
    <h1>Lista de productos</h1>
    <a href="/products/create">Nuevo</a>
    <hr>
    <table width="100%">
        <tr><th>Nombre</th><th>Precio</th><th>Categoria</th><th>Acciones</th></tr>
        @forelse ($products as $product)
        <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->price }}€</td>
            <td>{{ $product->cathegory->name }}</td>
            <td>
                <a class="btn btn-primary" href="/products/{{ $product->id }}">Ver</a>

                <a class="btn btn-warning" href="/products/{{ $product->id }}/edit">Editar</a>

                <a class="btn btn-success" href="/basket/{{ $product->id }}">Comprar</a>

                <form method="post" action="/products/{{ $product->id }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <input class="btn btn-danger" type="submit" value="Borrar">
                </form>
                <hr>
            </td>

        </tr>
    @empty
        <td>No hay productos!!</td>
    @endforelse
    </table>

    {{ $products->render() }}

@endsection
