@extends('layouts.app')

@section('title', 'Producto')

@section('content')
    <h1>Editar producto</h1>

    <form method="post" action="/products/{{ $product->id }}">
        {{ csrf_field() }}

        <input type="hidden" name="_method" value="PUT">

        <label>Nombre</label>
        <input type="text" name="name"
        value="{{ old('name') ? old('name') : $product->name }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>

        <br>

        <label>Precio</label>
        <input type="text" name="price"
        value="{{ old('price') ? old('price') : $product->price }}">
        <div class="alert alert-danger">
            {{ $errors->first('email') }}
        </div>
        <br>

        <?php
            use App\Cathegory;
            $cathegories = Cathegory::all();
        ?>
        <select name="cathegory_id">
        @foreach ($cathegories as $cathegory)
            <option value="{{ $cathegory->id }}"
            {{ $cathegory->id == $product->cathegory_id ?
            'selected="selected"' :
            ''
            }}>{{ $cathegory->name }}
        </option>
        @endforeach
        </select>
        <br>

        <input type="submit" value="Guardar Cambios">
    </form>
@endsection
