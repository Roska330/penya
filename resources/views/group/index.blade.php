@extends('layouts.app')

@section('content')
    <h1>Lista de usuarios del grupo </h1>
    <hr>
    <table width="100%">
        <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Rol</th>
        </tr>
        @forelse ($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->role->name }}</td>
        </tr>
    @empty
        <td>No hay usuarios!!</td>
    @endforelse

    </table>
    <hr>
    <a class="btn btn-danger" href="/groups/flush">Vaciar grupo</a>
@endsection
