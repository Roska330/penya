@extends('layouts.app')

@section('content')
    <h1>Lista de pedidos</h1>
    <hr>
    <table width="100%">
        <tr>
            <th>Usuario</th>
            <th>Precio</th>
            <th>Pagado</th>
            <th>Acciones</th>
        </tr>
        @forelse ($orders as $order)
        <tr>
            <td>{{ $order->user->name }}</td>
            <td>{{ $order->total() }}€</td>
            <td>{{ $order->pagado() }}</td>
            <td><a class="btn btn-primary" href="/orders/{{ $order->id }}">Ver</a></td>
            <td><a class="btn btn-primary" href="/orders/pdf/{{ $order->id }}">PDF</a></td>

        </tr>

    @empty
        <td>No hay pedidos!!</td>
    @endforelse
    </table>

    {{ $orders->render() }}

@endsection
