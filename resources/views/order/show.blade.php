@extends('layouts.app')

@section('title', 'Pedido')

@section('content')

    <h1>
        Este es el detalle del pedido <?php echo $order->id ?>
    </h1>
    {{-- {{ $order->products }} --}}
    Estado:
            {{ $order->pagado() }}
            <a class="btn btn-primary" href="/orders/{{ $order->id }}/update">Cambiar</a>
    <h1>Productos pedido</h1>
    <table width="100%">
        <tr>
            <th>Nombre</th>
            <th>Cantidad</th>
            <th>Precio</th>
            <th>Total</th>
        </tr>
        @forelse ($order->products as $product)
        <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->pivot->quantity }}</td>
            <td>{{ $product->pivot->price }}€</td>
            <td>{{ $product->price * $product->pivot->quantity }}€</td>

        </tr>
    @empty
        <td>No hay productos!!</td>
    @endforelse
    </table>


@endsection

