@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-primary text-white">Enviar pedido</div>

                <div class="card-body">
                    <form method="POST" action="{{url('mail/send')}}">
                        @csrf

                        <div class="form-group row">
                            <label for="subject" class="col.sm-4 col-form-label text-md-right">Titulo</label>
                            <div>
                                <input type="subject" name="subject" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col.sm-4 col-form-label text-md-right">Email</label>
                            <div>
                                <input type="email" name="email" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="message" class="col-sm-4 col-form-label text-md-right">Mensaje</label>
                            <div class="col-md-6">
                                <textarea class="form-control" name="message"></textarea>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>








</div>


@endsection
