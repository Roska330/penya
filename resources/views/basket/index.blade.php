@extends('layouts.app')

@section('content')
    <h1>Cesta compra</h1>
    <hr>
    <table width="100%">
        <tr>
            <th>Nombre</th>
            <th>Precio unitario</th>
            <th>Cantidad</th>
            <th>Precio total</th>
            <th>Acciones</th>
        </tr>
        @forelse ($products as $product)
        <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->quantity }}</td>
            <td>{{ $product->quantity * $product->price }} €</td>

            <td>
                |
                <a href="/basket/{{ $product->id }}/down">-</a>
                 |
                 <a href="/basket/{{ $product->id }}/up">+</a>
                 |

            {{ csrf_field() }}
            </td>
        </tr>
    @empty
        <td>No hay productos!!</td>
    @endforelse
    </table>
    <hr>
    <a class="btn btn-success" href="/basket/store">Completar</a>

@endsection
