@extends('layouts.app')

@section('content')
    <h1>Lista de usuarios</h1>
    <a href="/users/create">Nuevo</a>
    <hr>
    <table width="100%">
        <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Rol</th>
            <th>Acciones</th>
        </tr>
        @forelse ($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->role->name }}</td>

            <td>
                <a class="btn btn-primary" href="/users/{{ $user->id }}">Ver</a>

                @can("update",$user)
                    <a class="btn btn-warning" href="/users/{{ $user->id }}/edit">Editar</a>
                @endcan

                {{ csrf_field() }}

                @can("delete",$user)
                    <form method="post" action="/users/{{ $user->id }}">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Borrar" class="btn btn-danger">

                </form>
                @endcan

                <a class="btn btn-success" href="/groups/{{ $user->id }}">Guardar</a>
                <hr>
            </td>
        </tr>

    @empty
        <td>No hay usuarios!!</td>
    @endforelse
    </table>
    {{-- <ul>
    @forelse ($users as $user)
        <li>{{ $user->name }}: {{ $user->email }}
            <a href="/users/{{ $user->id }}/edit">Editar</a>

            <form method="post" action="/users/{{ $user->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar">
            </form>
        </li>
    @empty
        <li>No hay usuarios!!</li>
    @endforelse
    </ul> --}}

    {{ $users->render() }}

@endsection
