@extends('layouts.app')

@section('content')
    <h1>Lista de roles</h1>
    <a href="/roles/create">Nuevo</a>
    <hr>
    <table width="100%">
        <tr>
            <th>Nombre</th>

        </tr>
        @forelse ($roles as $role)
        <tr>
            <td>{{ $role->name }}</td>
            <td>
                @can("update",$role)
                    <a href="/roles/{{ $role->id }}/edit">Editar</a> |
                @endcan

                @can("view",$role)
                <a href="/users/{{ $role->id }}">Ver</a>
                @endcan

                {{ csrf_field() }}

                @can("delete",$role)
                    <form method="post" action="/users/{{ $role->id }}">
                    <input type="hidden" name="_method" value="DELETE">
                    |<input type="submit" value="borrar">

                </form>
                @endcan

            </td>
        </tr>
    @empty
        <td>No hay roles!!</td>
    @endforelse
    </table>

    {{ $roles->render() }}

@endsection
